var express = require('express');
var router = express.Router();

const TEST_TOKEN = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqd3RzZXJ2aWNlcyIsImV4cCI6MTU4MTQ1MjAwOSwiaWF0IjoxNTgxNDUwMjA5fQ.DoUxfeGRr9ztQ1ataydg3NlfwP2AHBKRu-wvVKg-PSyYDH9X6VDoQwwcUv1L0PlHRKTNFQ5mC5pEuOUNHIlqtA";
const DOMAINS = [
    { id: 1, name: 'web.com', reserve: 7200, bids: 1, highestBid: 100, status: 'Active', startDate: '2019-11-15 12:00:00 EST', endDate: '2020-06-15 12:00:00 EST' },
    { id: 2, name: 'etrade.com', reserve: 4500, bids: 0, highestBid: 0, status: 'Pending Approval', startDate: '2020-03-15 12:00:00 EST', endDate: '2020-06-15 12:00:00 EST' },
    { id: 3, name: 'facebook.com', reserve: 20000, bids: 43, highestBid: 16430, status: 'Scheduled', startDate: '2020-04-15 12:00:00 EST', endDate: '2020-06-15 12:00:00 EST' },
    { id: 4, name: 'cnn.com', reserve: 8900, bids: 12, highestBid: 4500, status: 'Ready for Disbursement', startDate: '2020-05-15 12:00:00 EST', endDate: '2020-06-15 12:00:00 EST' },
    { id: 5, name: 'sold.us', reserve: 1500, bids: 5, highestBid: 1200, status: 'Awaiting Transfer', startDate: '2019-11-15 12:00:00 EST', endDate: '2020-01-10 12:00:00 EST' },
    { id: 6, name: 'orange.ca', reserve: 1000, bids: 1, highestBid: 100, status: 'Transferred', startDate: '2019-11-15 12:00:00 EST', endDate: '2020-01-01 12:00:00 EST' }
];

/* GET domains */
router.post('/listDomains', function(req, res, next) {
    if (req.headers.authorization === TEST_TOKEN) {
        res.send(JSON.stringify(DOMAINS));
        return;
    }

    res.send('Error! No valid token provided');
});

module.exports = router;